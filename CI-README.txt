1.  Add composer.json and lock file in order to install any tools needed for
    testing. The vendor directory should be ignored from vcs.

2.  Add "build" directory in docroot. Configure so that a basic behat test can
    be run by navigating to {docroot}/buid/behat and running
    `../../vendor/bin/behat`.

    A default.behat.local.yml is added to the behat directory to help get you
    started with local behat setup. This needs to be copied to
    {docroot}/build/behat/behat.local.yml, and variables need to be configured
    so that behat knows how to test against your local site.

3.  Add composer scripts to make setup from nearly empty db easy, and run
    testing and setup commands.

4.  Set up codeship so that it can build a site that is prepared to be tested
    against.

    See https://codeship.com/projects/130523/configure_tests

    For this step, we are only interested in the "Setup Commands."
    We should be able to get through all of the setup commands without failure,
    which will result in a green build.

    Here is an example codeship setup script that works with this project:

    (Note that even though we are using services that require authentication such
    as amazon s3 and github authentication, none of the credentials are stored
    in the setup script itself. They are instead added as environment variables.
    This makes it safe to switch from using the codeship-provided ui for
    configuring tests to something like a codeship.yml file that is kept within
    the project repo itself.)

```
# Set php version through phpenv. 5.3, 5.4, 5.5 & 5.6 available
phpenv local 5.5
# Install awscli
pip install awscli
# Install dependencies through Composer
composer self-update
composer config -g github-oauth.github.com $GITHUB_ACCESS_TOKEN
composer install --prefer-dist --no-interaction
# Add composer bin to path
export PATH="$PATH:$(pwd)/vendor/bin"
# Setup database.
aws s3 cp s3://backpublish/$S3_DIR/$TEST_DATABASE /tmp
cp sites/default/default.settings.php sites/default/settings.php
echo "\$databases['default']['default']=array('driver' => 'mysql', 'host' => 'localhost', 'database' => 'test$TEST_ENV_NUMBER', 'username' => '$MYSQL_USER', 'password' => '$MYSQL_PASSWORD', 'prefix' => '');" >> sites/default/settings.php
# Pull down and install db from S3
drush sqlc < /tmp/$TEST_DATABASE
composer prepare-site
# Setup local php server
nohup php -S localhost:8000  > /dev/null 2>&1 &
# Set up behat params
cp build/behat/behat.codeship.yml build/behat/behat.local.yml

```

5.  Add testing steps to codeship configuration.

    If composer.json is configured with reusable scripts like the demo in this
    repo then this should just mean adding `composer test-all` to the
    test pipeline section

6.  Continue to add tests as new features and content types are added. When
    regressions are found and/or reported, then add tests for those too, so they
    don't unexpectedly reappear.
