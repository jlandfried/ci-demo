<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\CustomSnippetAcceptingContext;
use Behat\Behat\Tester\Exception;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Hook\Scope\AfterScenarioScope;

class FeatureContext extends RawDrupalContext implements CustomSnippetAcceptingContext {
  protected $originalPublicFilesDir = NULL;

  public static function getAcceptedSnippetType() {
    return 'regex';
  }
}
