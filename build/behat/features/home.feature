Feature: Anonymous users should see "Continuously integrated" on the homepage

  Scenario: As an anonymous user I visit the homepage and see "Continuously integrated"
    Given I am on the homepage
    Then I should see "Continuously integrated"
